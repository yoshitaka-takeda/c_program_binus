#include<stdio.h>
#include<stdlib.h>

int main()
{
    int n,i = 0;
    scanf("%d", &n);

    scanf("%*[\n]"); 
    /*this will read the \n in stdin and not store it anywhere. So the next call to 
     * scanf will not be interfered with */

    char **inputs;
    inputs = malloc(n * sizeof(char *));

    for (i = 0; i < n; i++)
    {
       inputs[i] = malloc(100 * sizeof(char));
    }


    for(i = 0; i < n; i++)
    {
        scanf("%*[\n]");
        scanf("%100[^\n]", inputs[i]);
    }

    for(i = 0; i < n; i++)
    {
        printf("%s\n", inputs[i]);
    }

    return 0;
}