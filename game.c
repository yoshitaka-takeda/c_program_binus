#include<stdio.h>
#include<ctype.h>
#include<stdlib.h>
#include<stdbool.h>

void quit()  // write error message and quit
{
    fprintf(stderr," memory exhausted\n") ;
    exit(1) ;
}

int main()
{
    int clen,qcase,s;

    scanf("%d %d",&clen,&qcase);

    char* arr=(char*)malloc(clen);

    if(arr==0) quit();
    
    while (true) {  // skip leading whitespace
        int c = getchar();
        if (c ==  EOF) break;  // end of file
        if (!isspace(c)) {
            ungetc(c,stdin);
            break;
        }
    }
    int i=0;
    while (true) {
        int c = getchar() ;
        if (c == '\n' || c ==  EOF) {  // at end; add terminating zero
            arr[i] = 0;
            break;
        }
        arr[i] = c;
        
        if (i== clen-1) {  // buffer full
            break;
            /*
            clen =  clen+clen;
            arr = (char*)realloc(arr, clen) ; // get a new and larger buffer
            if (arr == 0) quit() ;
            */
        }
        i++;
    }
    printf("%s\n", arr);

    for(s=0;s<clen;s++)
    {
        if('a'<=arr[s]&&arr[s]<='z')
        {
            arr[s]=toupper(arr[s]);
        }else if('A'<=arr[s]&&arr[s]<='Z')
        {
            arr[s]=tolower(arr[s]);
        }
    }

    printf("%s\n", arr);

    free(arr);
}