#include<stdio.h>
#include<ctype.h>
#include<stdlib.h>
#include<stdbool.h>

void quit()  // write error message and quit
{
    fprintf(stderr," memory exhausted\n") ;
    exit(1) ;
}

int main()
{
    int  max = 20;
    char* name = (char*) malloc(max) ;  // allocate buffer
    if (name == 0) quit();
    printf("Please enter your first name:\n");
    while (true) {  // skip leading whitespace
        int c = getchar();
        if (c ==  EOF) break;  // end of file
        if (!isspace(c)) {
            ungetc(c,stdin);
            break;
        }
    }
    int i = 0;
    while (true) {
        int c = getchar() ;
        if (c == '\n' || c ==  EOF) {  // at end; add terminating zero
            name[i] = 0;
            break;
        }
        name[i] = c;
        if (i== max-1) {  // buffer full
            max =  max+max;
            name = (char*)realloc(name, max) ; // get a new and larger buffer
            if (name == 0) quit() ;
        }
        i++;
    }
    printf("Hello %s\n",name);
    free(name) ;  // release memory
    return 0;
}