#include<stdio.h>
#include<stdlib.h>

#define MAX 9

int main()
{
    int i,j,k,tcase;

    scanf("%d",&tcase);

    char*** sbox = (char***)calloc(tcase,sizeof(char**));

    if (sbox==NULL)
    {
        fprintf(stderr, "Out of memory");
        exit(0);
    }

    for(i=0;i<tcase;i++)
    {
        sbox[i]=(char**)calloc(MAX,sizeof(char*));

        if(sbox[i]==NULL)
        {
            fprintf(stderr, "Out of memory");
            exit(0);
        }
        
        for (int j = 0; j < MAX; j++)
        {
            sbox[i][j]=(char*)calloc(MAX,sizeof(char));

            if (sbox[i][j] == NULL)        
            {
                fprintf(stderr, "Out of memory");
                exit(0);
            }
        }

    }

    i=0;
    for (int j = 0; j < MAX; j++)
    {
        for (int k = 0; k < MAX; k++)
        {
            scanf("%*[\n]");
            scanf("%d ", &sbox[i][j][k]);
        }
        scanf("%*[\n]");
    }

    /*
    for (int i = 0; i < tcase; i++)
    {
        int flag=0;
        do{
            for (int j = 0; j < MAX; j++)
            {
                for (int k = 0; k < MAX; k++)
                {
                    scanf("%d ", &sbox[i][j][k]);
                    getchar();
                    if(sbox[i][j][k]="\0")
                    {
                        flag+=1;
                    }
                }
                if(flag==1)
                {
                    flag=0;
                }
            }
        }while(flag<2);
    }
    printf("\n");
    */

    for (int i = 0; i < tcase; i++)
    {
        for (int j = 0; j < MAX; j++)
        {
            for (int k = 0; k < MAX; k++) {
                printf("%d ", sbox[i][j][k]);
            }
            printf("\n");
        }
        printf("\n");
    }

    // deallocate memory
    for (int i = 0; i < MAX; i++)
    {
        for (int j = 0; j < MAX; j++) {
            free(sbox[i][j]);
        }
        free(sbox[i]);
    }
    free(sbox);

    return 0;
}